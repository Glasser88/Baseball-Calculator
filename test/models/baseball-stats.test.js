'use strict';

const expect = require('chai').expect;
const battingStats = require('../../lib/models/baseball-stats');

const yoenisCespedes = {
  hits: 66,
  walks: 14,
  atBats: 230,
  avg: 0.287,
  hitByPitch: 4,
  sacFlies: 1,
  obp: 0.337,
  totalBases: 139,
  slg: 0.604,
}

describe('batting average', () => {
  it('divides the hits with at bats', () => {
    const hits = yoenisCespedes.hits;
    const atBats = yoenisCespedes.atBats;
    const expected = yoenisCespedes.avg;
    expect(battingStats.battingAverage(hits, atBats)).to.equal(expected);
  })
})

describe('on base percentage', () => {
  it('adds hits, walks and hitByPitch and divides them with atBats, walks, hitByPitch, and sacFlies', () => {
    const hits = yoenisCespedes.hits;
    const walks = yoenisCespedes.walks;
    const hitByPitch = yoenisCespedes.hitByPitch;
    const atBats = yoenisCespedes.atBats;
    const sacFlies = yoenisCespedes.sacFlies;
    const expected = yoenisCespedes.obp;
    expect(battingStats.onBasePercentage(hits, walks, hitByPitch, atBats, sacFlies)).to.equal(expected);
  })
})

describe('slugging percentage', () => {
  it('divides totalBases by atBats', () => {
    const totalBases = yoenisCespedes.totalBases;
    const atBats = yoenisCespedes.atBats;
    const expected = yoenisCespedes.slg;
    expect(battingStats.sluggingPercentage(totalBases, atBats)).to.equal(expected);
  })
})
