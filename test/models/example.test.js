'use strict';

const expect = require('chai').expect;
const example = require('../../lib/models/example');

describe('mulitiplier', () => {
  it('multiplies whats passed in', () => {
    const num = 3;
    const expected = 9;
    expect(example.mulitiplier(num)).to.equal(expected);
  })
})

describe('adder', () => {
  it('adds whats passed in', () => {
    const num = 3;
    const expected = 6;
    expect(example.adder(num)).to.equal(expected);
  })
})

describe('minuser', () => {
  it('subtracts whats passed in', () => {
    const num = 3;
    const expected = 0;
    expect(example.minuser(num)).to.equal(expected);
  })
})

describe('divider', () => {
  it('divides whats passed in', () => {
    const num = 3;
    const expected = 1;
    expect(example.divider(num)).to.equal(expected);
  })
})
