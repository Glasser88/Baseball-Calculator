module.exports = {
  battingAverage: (hits, atBats) => {
    let avg = hits / atBats;
    return Number(avg.toFixed(3));
  },
  onBasePercentage: (hits, walks, hitByPitch, atBats, sacFlies) => {
    let first = hits + walks + hitByPitch;
    let second = atBats + walks + hitByPitch + sacFlies;
    let obp = first / second;
    return Number(obp.toFixed(3));
  },
  sluggingPercentage: (totalBases, atBats) => {
    let slg = totalBases / atBats;
    return Number(slg.toFixed(3));
  }
}
