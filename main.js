battingAverage = () => {
  let hits = document.getElementById('hits').value;
  let atBats = document.getElementById('atBats').value;
  let avg = hits / atBats;
  document.getElementById('average').innerHTML = Number(avg.toFixed(3));
  document.getElementById('hits').value = '';
  document.getElementById('atBats').value = '';
}

onBasePercentage = () => {
  let hits = document.getElementById('obpHits').value;
  let walks = document.getElementById('obpWalks').value;
  let hitByPitch = document.getElementById('obpHitByPitch').value;
  let atBats = document.getElementById('obpAtBats').value;
  let sacFlies = document.getElementById('obpSacFlies').value;
  let first = (Number(hits) + Number(walks) + Number(hitByPitch));
  var second = (Number(atBats) + Number(walks) + Number(hitByPitch) + Number(sacFlies));
  var obp = first / second;
  document.getElementById('onbase').innerHTML = Number(obp.toFixed(3));
  document.getElementById('obpHits').value = '';
  document.getElementById('obpWalks').value = '';
  document.getElementById('obpHitByPitch').value = '';
  document.getElementById('obpAtBats').value = '';
  document.getElementById('obpSacFlies').value = '';
}

sluggingPercentage = () => {
  let totalBases = document.getElementById('sTotalBases').value;
  let atBats = document.getElementById('sAtBats').value;
  let slg = totalBases / atBats;
  document.getElementById('slugging').innerHTML = Number(slg.toFixed(3));
}
